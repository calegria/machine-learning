/*
https://github.com/arduino/ArduinoCore-mbed/blob/main/libraries/MLC/examples/RP2040_DataLogger_FIFO/RP2040_DataLogger_FIFO.ino
   This example exposes the first MB of Rp2040 flash as a USB disk.
   The user can interact with this disk as a bidirectional communication with the board
   For example, the board could save data in a file to be retrieved later with a drag and drop.
   If the user does a double tap, the firmware goes to datalogger mode (green led on).
   Now the user can do another double tap to start a recording of the IMU data 
   (green led blinking). With another double tap the recording will be stopped (green led on).
   Now the user can start/stop other recordings of the IMU data using again the double tap.
   The log files are saved in flash with an increasing number suffix data_0.txt, data_1.txt, etc.
   If you want to transfer the log files to the PC, you can reset the board and
   wait for 10 seconds (blue led blinking).
   You can find the video tutorial on LSM6DSOX MLC at: https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-imu-advanced 
*/

#include "PluggableUSBMSD.h"
#include "FlashIAPBlockDevice.h"
#include "WiFiNINA.h"
#include "LSM6DSOXSensor.h"

#define INT_1 INT_IMU
#define SENSOR_ODR 104.0f // In Hertz
#define ACC_FS 2 // In g
#define GYR_FS 2000 // In dps
#define MEASUREMENT_TIME_INTERVAL (1000.0f/SENSOR_ODR) // In ms
#define FIFO_SAMPLE_THRESHOLD 199
#define FLASH_BUFF_LEN 8192

// The number of milliseconds on ten seconds.
constexpr uint16_t kTenSecondsMillis = 10000u;
// The number of milliseconds on fifteen seconds.
constexpr uint16_t kFifteenSecondsMillis = 15000u;
// The number of milliseconds on fifteen seconds.
constexpr uint16_t kFiveSecondsMillis = 5000u;
// The number of milliseconds in which we will gather samples.
constexpr uint16_t kRecordMillisecondsThreshold = kFiveSecondsMillis;

// The number of milliseconds when demo_state changes to the DATA_LOGGER_RUNNING_STATE state.
uint16_t gDataLoggingStartMiliseconds = 0u;

typedef enum {
  DATA_STORAGE_STATE,
  DATA_LOGGER_IDLE_STATE,
  DATA_LOGGER_RUNNING_STATE
} demo_state_e;

// The current state.
volatile demo_state_e demo_state = DATA_STORAGE_STATE;
// Flag to indicate that a user event has happened. Is equal to 1 if event is present.
volatile int mems_event = 0;
uint32_t file_count = 0;
unsigned long timestamp_count = 0;
bool acc_available = false;
bool gyr_available = false;
int32_t acc_value[3];
int32_t gyr_value[3];
char buff[FLASH_BUFF_LEN];
uint32_t pos = 0;

static FlashIAPBlockDevice bd(XIP_BASE + 0x100000, 0x100000);

// Communication with the sensors.
LSM6DSOXSensor AccGyr(&Wire, LSM6DSOX_I2C_ADD_L);

// Activate of the USB mass storage interface.
USBMSD MassStorage(&bd);

// Thread object to change the LED light according to the current command status.
rtos::Thread acquisition_th;

FILE *f = nullptr;

/**
 * Set the flag that indicates a command event occured.
 */
void INT1Event_cb()
{
  mems_event = 1;
}

/**
 * Rewrite of USBMSD::begin(), which mounts the USB storage interface.
 */
void USBMSD::begin()
{
  int err = getFileSystem().mount(&bd);
  if (err)
  {
    err = getFileSystem().reformat(&bd);
  }
}

/**
 * Rewrite of USBMSD::getFileSystem(), which a reference to the local file system.
 */
mbed::FATFileSystem &USBMSD::getFileSystem()
{
  static mbed::FATFileSystem fs("fs");
  return fs;
}

/**
 * Blinks the Arduino green LED while tue current state is DATA_LOGGER_RUNNING_STATE.
 */
void led_green_thd()
{
  while (true)
  {
    if (demo_state != DATA_LOGGER_RUNNING_STATE)
      continue;

    // At this point, demo_state is eqal to DATA_LOGGER_RUNNING_STATE.
    digitalWrite(LEDG, HIGH);
    delay(100);
    digitalWrite(LEDG, LOW);
    delay(100);
  }
}

/**
 * Read the FIFO data from the sensors. The data is stored in the buff global variable.
 *
 ^ @param [in] samples_to_read The number of samples to read.
 */
void Read_FIFO_Data(const uint16_t samples_to_read)
{
  for (uint16_t iterationCounter=0u; iterationCounter<samples_to_read; ++iterationCounter)
  {
    uint8_t tag;
    // Check the FIFO tag
    AccGyr.Get_FIFO_Tag(&tag);
    switch (tag)
    {
      case LSM6DSOX_GYRO_NC_TAG:
      {
        // If we have a gyro tag, read the gyro data.
        AccGyr.Get_FIFO_G_Axes(gyr_value);
        gyr_available = true;
        break;
      }
      case LSM6DSOX_XL_NC_TAG:
      {
        // If we have an acc tag, read the acc data.
        AccGyr.Get_FIFO_X_Axes(acc_value);
        acc_available = true;
        break;
      }
      default:
        // We can discard other tags.
        break;
    }

    // If we have the measurements of both acc and gyro, we can store them with timestamp
    if (acc_available && gyr_available)
    {
      int num_bytes;
      num_bytes = snprintf(&buff[pos], (FLASH_BUFF_LEN - pos), "%lu %d %d %d %d %d %d\n", (unsigned long)((float)timestamp_count * MEASUREMENT_TIME_INTERVAL), (int)acc_value[0], (int)acc_value[1], (int)acc_value[2], (int)gyr_value[0], (int)gyr_value[1], (int)gyr_value[2]);
      pos += num_bytes;
      timestamp_count++;
      acc_available = false;
      gyr_available = false;
    }
  }

  // We can add the termination character to the string, so we are ready to save it in flash
  buff[pos] = '\0';
  pos = 0;
}

void setup()
{
  Serial.begin(115200);
  MassStorage.begin();
  pinMode(LEDB, OUTPUT);
  pinMode(LEDG, OUTPUT);
  digitalWrite(LEDB, LOW);
  digitalWrite(LEDG, LOW);

  // Initialize I2C bus.
  Wire.begin();
  Wire.setClock(400000);

  //Interrupts.
  attachInterrupt(INT_1, INT1Event_cb, RISING);

  // Initialize IMU.
  AccGyr.begin();
  AccGyr.Enable_X();
  AccGyr.Enable_G();
  // Configure ODR and FS of the acc and gyro
  AccGyr.Set_X_ODR(SENSOR_ODR);
  AccGyr.Set_X_FS(ACC_FS);
  AccGyr.Set_G_ODR(SENSOR_ODR);
  AccGyr.Set_G_FS(GYR_FS);
  // Enable the Double Tap event
  AccGyr.Enable_Double_Tap_Detection(LSM6DSOX_INT1_PIN);
  // Configure FIFO BDR for acc and gyro
  AccGyr.Set_FIFO_X_BDR(SENSOR_ODR);
  AccGyr.Set_FIFO_G_BDR(SENSOR_ODR);
  // Start Led blinking thread
  acquisition_th.start(led_green_thd);
}

void loop()
{
  // Handling event changes due to double tap on the sensor.
  if (mems_event)
  {
    // Turn off the command event flag.
    mems_event = 0;

    LSM6DSOX_Event_Status_t status;
    AccGyr.Get_X_Event_Status(&status);
    if (status.DoubleTapStatus)
    {
      // The user double tapped the sensors.
      // Switch on the state variable.
      switch (demo_state)
      {
        case DATA_STORAGE_STATE:
        {
          // Go to DATA_LOGGER_IDLE_STATE state.
          demo_state = DATA_LOGGER_IDLE_STATE;
          digitalWrite(LEDG, HIGH);
          Serial.println("From DATA_STORAGE_STATE To DATA_LOGGER_IDLE_STATE");
          break;
        }
        case DATA_LOGGER_IDLE_STATE:
        {
          char filename[32];
          // Go to DATA_LOGGER_RUNNING_STATE state.
          snprintf(filename, 32, "/fs/data_%lu.txt", file_count);
          Serial.print("Start writing file ");
          Serial.println(filename);
          // Open a file to write some data.
          // w+ means overwrite, so every time the board is rebooted the file will be overwritten.
          f = fopen(filename, "w+");
          if (f != nullptr)
          {
            // Write file header to the output file.
            fprintf(f, "Timestamp A_X A_Y A_Z G_X G_Y G_Z\n"); //Timestamp[ms] A_X [mg] A_Y [mg] A_Z [mg] G_X [mdps] G_Y [mdps] G_Z [mdps]
            fflush(f);
            Serial.println("From DATA_LOGGER_IDLE_STATE To DATA_LOGGER_RUNNING_STATE");
            Serial.print("FIFO samples = ");

            // Change the demo state.
            demo_state = DATA_LOGGER_RUNNING_STATE;
            // Tell the user the current state is DATA_LOGGER_RUNNING_STATE.
            digitalWrite(LEDG, LOW);
            // Set the current number of milliseconds to start counting the recording timeframe.
            gDataLoggingStartMiliseconds = millis();
            // Reset the filename generation suffix.
            timestamp_count = 0;
            // Reset the position of the storage buffer.
            pos = 0;
            // Reset the sensors data availability state.
            acc_available = false;
            gyr_available = false;
            // Set FIFO in Continuous mode.
            AccGyr.Set_FIFO_Mode(LSM6DSOX_STREAM_MODE);
          }
          break;
        }
        case DATA_LOGGER_RUNNING_STATE:
          Serial.println("Ignoring double tap on DATA_LOGGER_RUNNING_STATE state.");
          break;
        default:
          Serial.println("Error! Invalid state");
      }
    }
  }

  // Handling DATA_LOGGER_RUNNING_STATE state.
  if (demo_state == DATA_LOGGER_RUNNING_STATE)
  {
    // Check the number of samples inside the sensor FIFO.
    uint16_t fifo_samples;
    AccGyr.Get_FIFO_Num_Samples(&fifo_samples);
    Serial.print("+");

    // The number of milliseconds passed since the start of the recording.
    const uint16_t passedMilliseconds = millis() - gDataLoggingStartMiliseconds;

    // If the recording process is still active, then empty the FIFO only of the threshold has been reached.
    if (passedMilliseconds <= kRecordMillisecondsThreshold)
    {
      if (fifo_samples > FIFO_SAMPLE_THRESHOLD)
      {
        Serial.println();
        Serial.print("On record: Saving ");
        Serial.print(fifo_samples);
        Serial.println(" samples.");
        Serial.println("FIFO samples: ");
  
        // Empty the FIFO.
        Read_FIFO_Data(fifo_samples);
        // Store the string in flash memory.
        fprintf(f, "%s", buff);
        fflush(f);
      }

      // Leave the loop.
      return;
    }
    
    // Otherwise, empty the FIFO and finish the recorsing process.

    Serial.println();
    Serial.print("Final: Saving ");
    Serial.print(fifo_samples);
    Serial.println(" samples.");

    // Empty the FIFO.
    Read_FIFO_Data(fifo_samples);
    // Store the string in flash
    fprintf(f, "%s", buff);
    fflush(f);

    // Close the log file and increase the counter.
    fclose(f);
    file_count++;
    // Set FIFO in Bypass mode.
    AccGyr.Set_FIFO_Mode(LSM6DSOX_BYPASS_MODE);
    // Go to DATA_LOGGER_IDLE_STATE state.
    demo_state = DATA_LOGGER_IDLE_STATE;
    // Wait for the led thread ends the blinking
    delay(250);
    digitalWrite(LEDG, HIGH);
    Serial.println("From DATA_LOGGER_RUNNING_STATE To DATA_LOGGER_IDLE_STATE");

    // Leave the loop.
    return;
  }

  // Handling DATA_STORAGE_STATE state.
  if (demo_state == DATA_STORAGE_STATE && millis() > kTenSecondsMillis)
  {
    // At least ten seconds have passed on the DATA_STORAGE_STATE state.
    // Hence we stay in Mass Storage mode.

    // Disable the sensors.
    AccGyr.Disable_Double_Tap_Detection();
    AccGyr.Disable_X();
    AccGyr.Disable_G();

    // Blink the LED in blue to let the user node that we are in Mass Storage mode.
    while (true)
    {
      digitalWrite(LEDB, HIGH);
      delay(100);
      digitalWrite(LEDB, LOW);
      delay(100);
    }
  }
}